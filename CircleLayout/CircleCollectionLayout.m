//
//  CircleCollectionLayout.m
//  CircleLayout
//
//  Created by James Cash on 16-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "CircleCollectionLayout.h"

@interface CircleCollectionLayout ()

@property (nonatomic,strong) NSArray<NSArray<NSValue*>*>* points;

@end

@implementation CircleCollectionLayout

- (void)prepareLayout
{
    CGFloat midX = CGRectGetMidX(self.collectionView.frame);
    CGFloat midY = CGRectGetMidY(self.collectionView.frame);

    CGFloat sectionRadius = 150;

    NSMutableArray *sections = [[NSMutableArray alloc] init];
    NSInteger nsections = self.collectionView.numberOfSections;
    for (int section = 0; section < nsections; section++) {
        NSMutableArray *sectionPoints = [[NSMutableArray alloc] init];
        NSInteger nItems = [self.collectionView numberOfItemsInSection:section];
        CGFloat θ = (2 * M_PI) / nItems;
        for (int item = 0; item < nItems; item++) {
            NSInteger x = midX + sectionRadius * (section + 1) * cos(θ * item);
            NSInteger y = midY + sectionRadius * (section + 1) * sin(θ * item);
            CGPoint itemPoint = CGPointMake(x, y);
            [sectionPoints addObject:[NSValue valueWithCGPoint:itemPoint]];
        }
        [sections addObject:[NSArray arrayWithArray:sectionPoints]];
    }

    self.points = [NSArray arrayWithArray:sections];
}

- (CGSize)collectionViewContentSize
{
    return self.collectionView.bounds.size;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewLayoutAttributes *attrs = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];

    NSValue *wrappedPoint = self.points[indexPath.section][indexPath.item];
    CGPoint location = [wrappedPoint CGPointValue];
    attrs.center = location;

    attrs.size = CGSizeMake(50, 50);

    return attrs;
}

- (NSArray<UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSMutableArray *attrs = [[NSMutableArray alloc] init];
    for (int section = 0; section < self.collectionView.numberOfSections; section++) {
        for (int item = 0; item < [self.collectionView numberOfItemsInSection:section]; item++) {
            [attrs addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:item inSection:section]]];
        }
    }
    return attrs;
}

@end
