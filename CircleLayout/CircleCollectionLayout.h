//
//  CircleCollectionLayout.h
//  CircleLayout
//
//  Created by James Cash on 16-11-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleCollectionLayout : UICollectionViewLayout

@end
